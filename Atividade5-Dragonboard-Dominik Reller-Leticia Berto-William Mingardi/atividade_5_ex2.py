# -*- coding: utf-8 -*-

# Letícia Berto 587354
# Dominik Reller 587516
# William Mingardi 587362

import sys
import time
import json
import spidev
import DeviceClient


# Dragonboard
from libsoc_zero.GPIO import Button, Tilt
from time import sleep
from libsoc import gpio

spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=10000
spi.mode = 0b00
spi.bits_per_word = 8
channel_select=[0x01, 0xA0, 0x00]

# Set up the required variables for Azure IoT Hub
Azure_DeviceName = "iot"
Azure_DeviceKey = "lZBn+x4aZTIQIjMfAeUmLdwUNUavDiotx5pT7XZ7Wpk="
Azure_IoTHubName = "iotclass123"

if sys.version_info[0] == 3:
    input_func = input
else:
    input_func = raw_input

button = Button('GPIO-A')

def send_message(temp, touch):
    Json_Message = {"temperature" : temp, "touch" : touch}
    Encoded_Message = json.dumps(Json_Message).encode('utf8')
    Device = DeviceClient.DeviceClient(Azure_IoTHubName.lower(), Azure_DeviceName, Azure_DeviceKey)
    print(Encoded_Message)
    Device.create_sas(600)
    Azure_Sender = Device.send(Encoded_Message)
    if Azure_Sender == 204:
      print('Sucess')
    else:
      print('Failure')


if __name__=='__main__':
    gpio_cs = gpio.GPIO(18, gpio.DIRECTION_OUTPUT)
    with gpio.request_gpios([gpio_cs]):
        while True:
            #detectar temperatura
            gpio_cs.set_high()
            sleep(0.00001)
            gpio_cs.set_low()
            rx = spi.xfer(channel_select)
            gpio_cs.set_high()

            adc_value = (rx[1] << 8) & 0b1100000000
            adc_value = adc_value | (rx[2] & 0xff)

            # Transformacao para celsius
            adc_value = (adc_value - 32)/1.8

            print("adc_value: %d" % adc_value)
            #send_message_temp(str(adc_value))

            #detecta o touch e manda a mensagem
            if button.is_pressed():
                print("button_value: 1")
                send_message(str(adc_value), "1");

            else:
                print("button_value: 0")
                send_message(str(adc_value), "0");
            time.sleep(5)
