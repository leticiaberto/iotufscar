# -*- coding: utf-8 -*-

# Letícia Berto 587354
# Dominik Reller 587516
# William Mingardi 587362

import paho.mqtt.client as mqtt
import sys
import time
import json
import spidev

# Bluemix
import ibmiotf.application

# Dragonboard
from libsoc_zero.GPIO import Button, Tilt
from time import sleep
from libsoc import gpio

spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=10000
spi.mode = 0b00
spi.bits_per_word = 8
channel_select=[0x01, 0xA0, 0x00]

if sys.version_info[0] == 3:
    input_func = input
else:
    input_func = raw_input

button = Button('GPIO-A')

#Set the variables for connecting to the iot service
broker = ""
topic = "iot-2/evt/status/fmt/json"
username = "use-token-auth"
password = "D1C07mjhB4N!qVFvp9" #auth-token
organization = "424kq1" #org_id
deviceType = "dragonboard410c"
deviceId = "dragonboard410cID"

topic = "iot-2/evt/status/fmt/json"

#Creating the client connection
#Set clientID and broker
clientID = "d:" + organization + ":" + deviceType + ":" + deviceId
broker = organization + ".messaging.internetofthings.ibmcloud.com"
mqttc = mqtt.Client(clientID)

#Set authentication values, if connecting to registered service
if username is not "":
    mqttc.username_pw_set(username, password=password)

mqttc.connect(host=broker, port=1883, keepalive=60)

def send_message_touch(msg):
    msg = json.JSONEncoder().encode({"d":{"touch":msg}})
    mqttc.publish(topic, payload=msg, qos=0, retain=False)

def send_message_temp(msg):
    msg = json.JSONEncoder().encode({"d":{"temp":msg}})
    mqttc.publish(topic, payload=msg, qos=0, retain=False)

def detectaTemperatura(gpio_cs):
    gpio_cs.set_high()
    sleep(0.00001)
    gpio_cs.set_low()
    rx = spi.xfer(channel_select)
    gpio_cs.set_high()

    adc_value = (rx[1] << 8) & 0b1100000000
    adc_value = adc_value | (rx[2] & 0xff)

    # Transformacao para celsius
    adc_value = (adc_value - 32)/1.8

    print("adc_value: %d" % adc_value)
    send_message_temp(str(adc_value))

def detectaTouch():
    if button.is_pressed():
        print("button_value: 1")
        send_message_touch(1)
    else:
        print("button_value: 0")
        send_message_touch(0)

if __name__=='__main__':
    gpio_cs = gpio.GPIO(18, gpio.DIRECTION_OUTPUT)
    with gpio.request_gpios([gpio_cs]):
        mqttc.loop_start()
        while mqttc.loop() == 0:

            detectaTemperatura(gpio_cs)
            detectaTouch()

            time.sleep(5)
            pass
