# -*- coding: utf-8 -*-

# Letícia Berto 587354
# Dominik Reller 587516
# William Mingardi 587362

import paho.mqtt.client as mqtt
import sys
import time
import json
import spidev
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient


# Dragonboard
from libsoc_zero.GPIO import Button, Tilt
from time import sleep
from libsoc import gpio

spi = spidev.SpiDev()
spi.open(0,0)
spi.max_speed_hz=10000
spi.mode = 0b00
spi.bits_per_word = 8
channel_select=[0x01, 0xA0, 0x00]

if sys.version_info[0] == 3:
    input_func = input
else:
    input_func = raw_input

button = Button('GPIO-A')

host = "aq9vt61ndc7ki.iot.us-west-2.amazonaws.com"
rootCAPath = "root-CA.crt"
certificatePath = "dragonboard410c.cert.pem"
privateKeyPath = "dragonboard410c.private.key"
useWebsocket = False
clientId = "dragonboard410c"
topic = "iot"

# Init AWSIoTMQTTClient
myAWSIoTMQTTClient = None
if useWebsocket:
	myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId, useWebsocket=True)
	myAWSIoTMQTTClient.configureEndpoint(host, 443)
	myAWSIoTMQTTClient.configureCredentials(rootCAPath)
else:
	myAWSIoTMQTTClient = AWSIoTMQTTClient(clientId)
	myAWSIoTMQTTClient.configureEndpoint(host, 8883)
	myAWSIoTMQTTClient.configureCredentials(rootCAPath, privateKeyPath, certificatePath)

# AWSIoTMQTTClient connection configuration
myAWSIoTMQTTClient.configureAutoReconnectBackoffTime(1, 32, 20)
myAWSIoTMQTTClient.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
myAWSIoTMQTTClient.configureDrainingFrequency(2)  # Draining: 2 Hz
myAWSIoTMQTTClient.configureConnectDisconnectTimeout(10)  # 10 sec
myAWSIoTMQTTClient.configureMQTTOperationTimeout(5)  # 5 sec

# Connect and subscribe to AWS IoT
myAWSIoTMQTTClient.connect()

def send_message_touch(msg):
    myAWSIoTMQTTClient.publish(topic + "/touch", msg, 1)

def send_message_temp(msg):
    myAWSIoTMQTTClient.publish(topic + "/temp", msg, 1)

def detectaTemperatura(gpio_cs):
    gpio_cs.set_high()
    sleep(0.00001)
    gpio_cs.set_low()
    rx = spi.xfer(channel_select)
    gpio_cs.set_high()

    adc_value = (rx[1] << 8) & 0b1100000000
    adc_value = adc_value | (rx[2] & 0xff)

    # Transformacao para celsius
    adc_value = (adc_value - 32)/1.8

    print("adc_value: %d" % adc_value)
    send_message_temp(str(adc_value))

def detectaTouch():
    if button.is_pressed():
        print("button_value: 1")
        send_message_touch(1)
    else:
        print("button_value: 0")
        send_message_touch(0)

if __name__=='__main__':
    gpio_cs = gpio.GPIO(18, gpio.DIRECTION_OUTPUT)
    with gpio.request_gpios([gpio_cs]):
        while True:

            detectaTemperatura(gpio_cs)
            detectaTouch()

            time.sleep(5)
            pass
